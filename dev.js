const path = require('path');
const express = require('express');

const app = express();

const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 9000;

app.get('/bundle.js', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist', 'bundle.js'));
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

app.listen(port, host, (err) => {
    if (err) {
        console.error(err);
        return;
    }

    console.info(`Listening at http://${host}:${port}`);
});