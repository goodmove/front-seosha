# Seosha

This repository contains front for web service that analyzes text content of web pages.

# Сборка

Установить все зависимости
```
npm install
```

Собрать bundle.js
```
npm run-script build
```

# Локальный запуск

Для запуска express сервера на порту 9000:

```
npm start
```

Для удобства разработки рекомендуется запустить watcher изменений кода:

```
npm run watch
```