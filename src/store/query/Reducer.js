import Immutable from 'seamless-immutable';
import { types } from "src/store/query/Types";

const initialState = Immutable({
    analytics: undefined,
    query: undefined
});

export const getQueryAnalytics = state => state.query.analytics;
export const getQuery = state => state.query.query;

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.QUERY_ANALYTICS_FETCHED:
            return state.merge({
                analytics: action.analytics
            });

        case types.QUERY_CHANGED:
            return state.merge({
                query: action.query
            });

        default:
            return state;
    }
}