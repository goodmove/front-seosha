import AnalyticsService from "src/service/AnalyticsService";
import { types as queryTypes } from "src/store/query/Types";

export function fetchQueryAnalytics(query) {
    return dispatch => AnalyticsService.analyzeQuery(dispatch, query);
}

export const clearQueryAnalytics = () => dispatch => dispatch({
    type: queryTypes.QUERY_ANALYTICS_FETCHED,
    analytics: undefined
});

export const setQuery = (query) => dispatch => dispatch({
    type: queryTypes.QUERY_CHANGED,
    query
});