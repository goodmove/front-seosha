import keyMirror from 'key-mirror';

export const types = keyMirror({
    QUERY_ANALYTICS_FETCHED: null,
    QUERY_CHANGED: null
});