import keyMirror from 'key-mirror';

export const types = keyMirror({
    SITE_ANALYTICS_FETCHED: null,
    QUERY_CHANGED: null
});