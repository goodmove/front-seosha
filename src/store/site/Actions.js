import AnalyticsService from "src/service/AnalyticsService";
import { types as siteTypes, types } from "src/store/site/Types";

export const fetchSiteAnalytics = (url, keyWords) => dispatch => AnalyticsService.analyzeSite(dispatch, url, keyWords);

export const clearSiteAnalytics = () => dispatch => dispatch({
    type: siteTypes.SITE_ANALYTICS_FETCHED,
    analytics: undefined
});

export const setQuery = (url, keyWords) => dispatch => dispatch({
    type: types.QUERY_CHANGED,
    query: { url, keyWords }
});