import Immutable from 'seamless-immutable';
import { types } from "src/store/site/Types";

const initialState = Immutable({
    analytics: undefined,
    url: undefined,
    keyWords: undefined
});

export const getSiteAnalytics = state => state.site.analytics;
export const getUrl = state => state.site.url;
export const getKeyWords = state => state.site.keyWords;

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SITE_ANALYTICS_FETCHED:
            return state.merge({ analytics: action.analytics });

        case types.QUERY_CHANGED:
            return state.merge({
                url: action.query.url,
                keyWords: action.query.keyWords
            });

        default:
            return state;
    }
}