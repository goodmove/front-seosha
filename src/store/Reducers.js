import site from 'src/store/site/Reducer';
import query from 'src/store/query/Reducer';
import common from 'src/store/common/Reducer';
import { reducer as form } from 'redux-form';

export const reducers = { site, query, common, form };