import Immutable from 'seamless-immutable';
import DictionaryService from "src/service/DictionaryService";

const initialState = Immutable({
    dictionary: DictionaryService.getDictionary()
});

export const getDictionary = state => state.common.dictionary;

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        default:
            return state;
    }
}