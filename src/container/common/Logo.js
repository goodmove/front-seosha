import { getDictionary } from "src/store/common/Reducer";
import { connect } from "react-redux";
import React from "react";

import styles from "./Logo.scss";
import { Link } from "react-router-dom";
import { routes } from "src/model/Model";


const Logo = ({ dictionary }) => (
    <Link to={routes.INDEX} className={styles.logo}>{dictionary.logo.toLowerCase()}</Link>
);

const mapStateToProps = state => ({
    dictionary: getDictionary(state)
});

export default connect(mapStateToProps)(Logo);