import React from "react";

import styles from './Header.scss';
import { H1 } from "src/component/text/H1";
import { getDictionary } from "src/store/common/Reducer";
import { connect } from "react-redux";
import { Section } from "src/component/section/Section";
import { GradientBackground } from "src/component/section/GradientBackground";
import Logo from "src/container/common/Logo";

const Header = ({ dictionary }) => (
    <GradientBackground>
        <Section>
            <div className={styles.horizontal}>
                <Logo/>
                <H1>{dictionary.learnByWhichWordsTheyFindSite}</H1>
            </div>
        </Section>
    </GradientBackground>
);

const mapStateToProps = state => ({
    dictionary: getDictionary(state)
});

export default connect(mapStateToProps)(Header);