import * as React from "react";
import { getDictionary } from "src/store/common/Reducer";
import { connect } from "react-redux";
import Logo from "src/container/common/Logo";
import { H1 } from "src/component/text/H1";
import Button from "src/component/button/Button";
import { routes } from "src/model/Model";

import styles from "./MainPage.scss";
import { Section } from "src/component/section/Section";
import { GradientBackground } from "src/component/section/GradientBackground";

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.goto = this.goto.bind(this);
    }

    goto(route) {
        this.props.history.push(route);
    }

    render() {
        const { dictionary } = this.props;

        return (
            <div>
                <GradientBackground>
                    <Section>
                        <Logo/>
                        <H1>{dictionary.learnByWhichWordsTheyFindSite}</H1>
                        <div className={styles.buttonsContainer}>
                            <div className={styles.contentContainer}>
                                <H1>{dictionary.concreteSite}</H1>
                                <Button text={dictionary.research} onClick={() => this.goto(routes.SITE)}/>
                            </div>
                            <div className={styles.contentContainer}>
                                <H1>{dictionary.topSearchResults}</H1>
                                <Button text={dictionary.research}
                                        onClick={() => this.goto(routes.SEARCH_QUERY)}/>
                            </div>
                        </div>
                    </Section>
                </GradientBackground>

                <Section>
                    <div className={styles.title}>{dictionary.youWillLearn}</div>
                    <div className={styles.descriptionContainer}>
                        <div className={styles.contentContainer}>
                            <div className={styles.circle}>1</div>
                            <div className={styles.descriptionTitle}>{dictionary.siteAnalysis}</div>
                            <div className={styles.description}>{dictionary.siteAnalysisDescription}</div>
                        </div>
                        <div className={styles.contentContainer}>
                            <div className={styles.circle}>2</div>
                            <div className={styles.descriptionTitle}>{dictionary.queryAnalysis}</div>
                            <div className={styles.description}>{dictionary.queryAnalysisDescription}</div>
                        </div>
                        <div className={styles.contentContainer}>
                            <div className={styles.circle}>3</div>
                            <div className={styles.descriptionTitle}>{dictionary.nauseaAnalysis}</div>
                            <div className={styles.description}>{dictionary.nauseaAnalysisDescription}</div>
                        </div>
                    </div>
                </Section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dictionary: getDictionary(state)
});

export default connect(mapStateToProps)(MainPage);