import React from "react";
import { getAnalysisType, getDictionary } from "src/store/common/Reducer";
import { connect } from "react-redux";
import { routes } from "src/model/Model";
import { Link } from "react-router-dom";

import styles from "src/container/common/Toolbar.scss";
import Header from "src/container/common/Header";

class Toolbar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { dictionary } = this.props;
        const { pathname } = this.props?.history?.location;

        const toStyle = route => pathname === route ? styles.selected : styles.notSelected;

        return (
            <div>
                <Header/>
                <div className={styles.toolbar}>
                    <Link
                        className={toStyle(routes.SITE)}
                        to={routes.SITE}>
                        {dictionary.siteAnalysis}
                    </Link>
                    <Link
                        className={toStyle(routes.SEARCH_QUERY)}
                        to={routes.SEARCH_QUERY}>
                        {dictionary.queryAnalysis}
                    </Link>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dictionary: getDictionary(state)
});

export default connect(mapStateToProps)(Toolbar);