import * as React from "react";
import * as queryString from "query-string";
import { clearQueryAnalytics, fetchQueryAnalytics, setQuery } from "src/store/query/Actions";
import { getDictionary } from "src/store/common/Reducer";
import { connect } from "react-redux";
import { Processing } from "src/component/Processing";
import { Section } from "src/component/section/Section";
import { routes } from "src/model/Model";
import QueryAnalyticsResults from "src/container/query/QueryAnalyticsResults";
import { getQuery, getQueryAnalytics } from "src/store/query/Reducer";


class QueryAnalyticsResultsScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { query } = queryString.parse(this.props.location.search);
        if (!query) {
            return this.props.history.push(routes.SEARCH_QUERY);
        }
        this.props.dispatch(setQuery(query));
        this.props.dispatch(fetchQueryAnalytics(query));
    }

    componentWillUnmount() {
        this.props.dispatch(clearQueryAnalytics())
    }

    render() {
        const { analytics } = this.props;
        console.log(analytics);

        return (
            <div>
                <Section>
                    {
                        analytics ? <QueryAnalyticsResults/> : <Processing/>
                    }
                </Section>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    dictionary: getDictionary(state),
    analytics: getQueryAnalytics(state),
    query: getQuery(state)
});

export default connect(mapStateToProps)(QueryAnalyticsResultsScreen);