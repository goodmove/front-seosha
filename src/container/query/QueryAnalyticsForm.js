import * as React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { getDictionary } from "src/store/common/Reducer";
import Button, { types } from "src/component/button/Button";
import { Section } from "src/component/section/Section";

import styles from "src/container/common/Forms.scss"
import { required } from "src/component/field/Validators";
import { TextField } from "src/component/field/TextField";

export const FORM_NAME = 'QueryAnalyticsForm';

class QueryAnalyticsForm extends React.Component {
    render() {
        const { dictionary, onProceed, handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(onProceed)}>
                <Section>
                    <div className={styles.title}>{dictionary.queryAnalysis}</div>
                    <div className={styles.fieldContainer}>
                        <label htmlFor={'query'}>{dictionary.searchQuery}</label>
                        <Field
                            id={'query'}
                            name={'query'}
                            component={TextField}
                            dictionary={dictionary}
                            validate={required}
                        />
                        <div className={styles.hint}>{dictionary.weAnalyzeYandexTopSearchResults}</div>
                    </div>

                    <Button
                        type={types.SUBMIT}
                        text={dictionary.search}/>
                </Section>
            </form>
        );
    }
}

const mapStateToProps = state => {
    return { dictionary: getDictionary(state) }
};

export default connect(mapStateToProps)(reduxForm({ form: FORM_NAME })(QueryAnalyticsForm));