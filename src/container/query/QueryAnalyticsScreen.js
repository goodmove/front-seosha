import * as React from "react";
import { connect } from "react-redux";
import { getDictionary } from "src/store/common/Reducer";
import QueryAnalyticsForm from "src/container/query/QueryAnalyticsForm";
import { routes } from "src/model/Model";
import * as queryString from "query-string";
import { getQueryAnalytics } from "src/store/query/Reducer";


class QueryAnalyticsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onAnalyze = this.onAnalyze.bind(this);
    }

    onAnalyze({ query }) {
        this.props.history.push({
            pathname: routes.QUERY_RESULTS,
            search: queryString.stringify({ query })
        });
    }

    render() {
        const { dictionary, analytics } = this.props;

        return (
            <div>
                <QueryAnalyticsForm onProceed={this.onAnalyze}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dictionary: getDictionary(state),
    analytics: getQueryAnalytics(state)
});

export default connect(mapStateToProps)(QueryAnalyticsScreen);