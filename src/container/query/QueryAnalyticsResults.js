import * as React from "react";
import { getDictionary } from "src/store/common/Reducer";
import { connect } from "react-redux";
import { Cell, HeaderCell, HeaderRow, Row, Table } from "src/component/table/Table";

import styles from './QueryAnalyticsResults.scss';
import { H2 } from "src/component/text/H2";
import { H1 } from "src/component/text/H1";
import { getQuery, getQueryAnalytics } from "src/store/query/Reducer";
import { Card } from "src/component/section/Card";

const sameRoot = count => (count % 10 === 1 && count % 100 !== 11) ? 'однокоренное' : 'однокоренных';

const QueryInfo = ({ dictionary, query }) => (
    <div className={styles.queryInfoTable}>
        <Table>
            <Row>
                <Cell>{dictionary.searchQuery}</Cell>
                <Cell>{query}</Cell>
            </Row>
        </Table>
    </div>
);

const AnalyzedSites = ({ dictionary, details }) => (
    <Card>
        <div className={styles.analyzedSitesTable}>
            <H2>{dictionary.analyzedSites}</H2>
            <Table>
                {
                    details.map(d => <Row key={d.url}><Cell>{d.url}</Cell></Row>)
                }
            </Table>
        </div>
    </Card>
);


const LemmaRow = ({ dictionary, lemma }) => (
    <Row>
        <Cell>{lemma.lemma}</Cell>
        <Cell>{lemma.formsCount + lemma.count} / {lemma.count}</Cell>
        <Cell>
            {
                lemma.sameRoot?.length > 0 &&
                <div>
                    {
                        lemma.sameRoot
                            .filter(l => l?.word)
                            .map(l => <div
                                key={l.word}>{l.word.toLowerCase()} ({l.count})</div>)
                    }
                </div>
            }
        </Cell>
    </Row>
);

const LemmasTable = ({ title, dictionary, lemmas }) => {
    return (
        <Card>
            {
                lemmas.length > 0 &&
                <div className={styles.detailsTable}>
                    <H2>{title}</H2>
                    <Table>
                        <HeaderRow>
                            <HeaderCell>{dictionary.word}</HeaderCell>
                            <HeaderCell>{dictionary.numberOfOccurrences}</HeaderCell>
                            <HeaderCell>{dictionary.sameRoots}</HeaderCell>
                        </HeaderRow>
                        {
                            lemmas.slice(0, 20).map(lemma => <LemmaRow key={lemma.lemma} dictionary={dictionary}
                                                                       lemma={lemma}/>)
                        }
                    </Table>
                </div>
            }
        </Card>
    )
};

class QueryAnalyticsResults extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { analytics, dictionary, query } = this.props;

        return (
            <div>
                <H1>{dictionary.queryAnalysis}</H1>
                <QueryInfo dictionary={dictionary} query={query}/>
                <AnalyzedSites dictionary={dictionary} details={analytics.details}/>
                <LemmasTable title={dictionary.summary} dictionary={dictionary} lemmas={analytics.summary.words}/>

                <H1>{dictionary.detailsFromEachSite}</H1>
                {
                    analytics.details.map(d =>
                        <LemmasTable key={d.url} dictionary={dictionary} lemmas={d.lemmas} title={d.url}/>
                    )
                }
            </div>
        );
    }
}


const mapStateToProps = state => ({
    dictionary: getDictionary(state),
    analytics: getQueryAnalytics(state),
    query: getQuery(state)

});

export default connect(mapStateToProps)(QueryAnalyticsResults);