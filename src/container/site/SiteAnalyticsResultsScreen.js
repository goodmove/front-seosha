import * as React from "react";
import * as queryString from "query-string";
import { clearSiteAnalytics, fetchSiteAnalytics, setQuery } from "src/store/site/Actions";
import { getDictionary } from "src/store/common/Reducer";
import { getKeyWords, getSiteAnalytics, getUrl } from "src/store/site/Reducer";
import { connect } from "react-redux";
import { Processing } from "src/component/Processing";
import { Section } from "src/component/section/Section";
import SiteAnalyticsResults from "src/container/site/SiteAnalyticsResults";
import { routes } from "src/model/Model";


class SiteAnalyticsResultsScreen extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        const { url, keyWords } = queryString.parse(this.props.location.search);
        if (!url) {
            return this.props.history.push(routes.SITE);
        }
        this.props.dispatch(setQuery(url, keyWords));
        this.props.dispatch(fetchSiteAnalytics(url, keyWords));
    }

    componentWillUnmount() {
        this.props.dispatch(clearSiteAnalytics())
    }

    render() {
        const { analytics, dictionary } = this.props;

        return (
            <div>
                <Section>
                    {
                        analytics ? <SiteAnalyticsResults/> : <Processing/>
                    }
                </Section>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    dictionary: getDictionary(state),
    analytics: getSiteAnalytics(state),
    url: getUrl(state),
    keyWords: getKeyWords(state)
});

export default connect(mapStateToProps)(SiteAnalyticsResultsScreen);