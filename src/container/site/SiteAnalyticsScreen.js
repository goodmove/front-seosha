import * as React from "react";
import { connect } from "react-redux";
import { getSiteAnalytics } from "src/store/site/Reducer";
import { getDictionary } from "src/store/common/Reducer";
import SiteAnalyticsForm from "src/container/site/SiteAnalyticsForm";
import * as queryString from "query-string";
import { routes } from "src/model/Model";


class SiteAnalyticsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onAnalyze = this.onAnalyze.bind(this);
    }

    onAnalyze({ url, keyWords }) {
        this.props.history.push({
            pathname: routes.SITE_RESULTS,
            search: queryString.stringify({ url, keyWords })
        });
    }

    render() {
        const { dictionary, analytics } = this.props;

        return (
            <div>
                <SiteAnalyticsForm onProceed={this.onAnalyze}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dictionary: getDictionary(state),
    analytics: getSiteAnalytics(state)
});

export default connect(mapStateToProps)(SiteAnalyticsScreen);