import * as React from "react";
import { Field, formValueSelector, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { getDictionary } from "src/store/common/Reducer";
import Button, { types } from "src/component/button/Button";

import styles from "src/container/common/Forms.scss";
import { Section } from "src/component/section/Section";
import { TextField } from "src/component/field/TextField";
import { required, validateUrl } from "src/component/field/Validators";

export const FORM_NAME = 'SiteAnalyticsForm';

class SiteAnalyticsForm extends React.Component {
    render() {
        const { dictionary, onProceed, handleSubmit, url } = this.props;

        return (
            <form onSubmit={handleSubmit(onProceed)}>
                <Section>
                    <div className={styles.title}>{dictionary.siteAnalysis}</div>

                    <div className={styles.fieldContainer}>
                        <label htmlFor={'keyWords'}>{dictionary.siteUrl}</label>
                        <Field
                            id={'url'}
                            name={'url'}
                            placeholder={dictionary.insertSiteUrl}
                            component={TextField}
                            dictionary={dictionary}
                            type={'text'}
                            validate={[required, validateUrl]}
                        />
                    </div>

                    <div className={styles.fieldContainer}>
                        <label htmlFor={'keyWords'}>{dictionary.keywords}</label>
                        <Field
                            id={'keyWords'}
                            name={'keyWords'}
                            component={'textarea'}/>
                    </div>

                    <Button
                        type={types.SUBMIT}
                        text={dictionary.search}/>
                </Section>
            </form>
        );
    }
}

const selector = formValueSelector(FORM_NAME);

const mapStateToProps = state => {
    return {
        dictionary: getDictionary(state),
        url: selector(state, 'url')
    }
};

export default connect(mapStateToProps)(reduxForm({ form: FORM_NAME })(SiteAnalyticsForm));