import * as React from "react";
import { getDictionary } from "src/store/common/Reducer";
import { getKeyWords, getSiteAnalytics, getUrl } from "src/store/site/Reducer";
import { connect } from "react-redux";
import { Cell, HeaderCell, HeaderRow, Row, Table } from "src/component/table/Table";

import styles from './SiteAnalyticsResults.scss';
import { H1 } from "src/component/text/H1";
import { H2 } from "src/component/text/H2";

const sameRoot = count => (count % 10 === 1 && count % 100 !== 11) ? 'однокоренное' : 'однокоренных';

const QueryInfo = ({ dictionary, query }) => (
    <div className={styles.queryInfoTable}>
        <Table>
            <Row>
                <Cell>{dictionary.siteUrl}</Cell>
                <Cell>{query.url}</Cell>
            </Row>
            <Row>
                <Cell>{dictionary.keywords}</Cell>
                <Cell>{query.keyWords || dictionary.notSpecified}</Cell>
            </Row>
        </Table>
    </div>
);

const Lemma = ({ dictionary, lemma }) => (
    <Row>
        <Cell>{lemma.lemma}</Cell>
        <Cell>{lemma.formsCount + lemma.count} / {lemma.count}</Cell>
        <Cell>
            {
                lemma.sameRoot?.length > 0 &&
                <div>
                    {
                        lemma.sameRoot
                            .filter(l => l?.word)
                            .map(l => <div
                                key={l.word}>{l.word.toLowerCase()} ({l.count})</div>)
                    }
                </div>
            }
        </Cell>
    </Row>
);

const Details = ({ dictionary, analytics }) => {
    const keyWords = [...analytics.lemmas.filter(l => l.isKeyWord)]
        .sort((a, b) => (a.count + a.formsCount) < (b.formsCount + b.count) ? 1 : -1);

    const otherWords = [...analytics.lemmas.filter(l => !l.isKeyWord)]
        .sort((a, b) => (a.count + a.formsCount) < (b.formsCount + b.count) ? 1 : -1);

    return (
        <div>
            {
                keyWords.length > 0 &&
                <div className={styles.detailsTable}>
                    <H2>{dictionary.keywords}</H2>
                    <Table>
                        <HeaderRow>
                            <HeaderCell>{dictionary.word}</HeaderCell>
                            <HeaderCell>{dictionary.numberOfOccurrences}</HeaderCell>
                            <HeaderCell>{dictionary.sameRoots}</HeaderCell>
                        </HeaderRow>
                        {
                            keyWords.map(lemma => <Lemma key={lemma.lemma} dictionary={dictionary} lemma={lemma}/>)
                        }
                    </Table>
                </div>
            }

            {
                otherWords.length > 0 &&
                <div className={styles.detailsTable}>
                    {
                        keyWords.length > 0 && <H2>{dictionary.otherWords}</H2>
                    }
                    <Table>
                        <HeaderRow>
                            <HeaderCell>{dictionary.word}</HeaderCell>
                            <HeaderCell>{dictionary.numberOfOccurrences}</HeaderCell>
                            <HeaderCell>{dictionary.sameRoots}</HeaderCell>
                        </HeaderRow>
                        {
                            otherWords.map(lemma => <Lemma key={lemma.lemma} dictionary={dictionary} lemma={lemma}/>)
                        }
                    </Table>
                </div>
            }
        </div>
    )
};

class SiteAnalyticsResults extends React.Component {
    constructor(props) {
        super(props);
        this.copyToClipboard = this.copyToClipboard.bind(this);
    }

    copyToClipboard() {
        console.log(this.props.analytics);

        const el = document.createElement('textarea');
        // Set value (string to be copied)
        el.value = this.props.analytics.lemmas.map(lemma => lemma.lemma).join(', ');
        // Set non-editable to avoid focus and move outside of view
        el.setAttribute('readonly', '');
        document.body.appendChild(el);
        // Select text inside element
        el.select();
        // Copy text to clipboard
        document.execCommand('copy');
        // Remove temporary element
        document.body.removeChild(el);
    };

    render() {
        const { analytics, dictionary, url, keyWords, detailsOpen } = this.props;

        return (
            <div>
                <H1>{dictionary.siteAnalysis}</H1>

                <QueryInfo
                    dictionary={dictionary}
                    query={{ url, keyWords }}/>

                <div className={styles.container}>
                    <H1>{dictionary.results}</H1>
                    <div className={styles.button} onClick={this.copyToClipboard}>Скопировать</div>
                </div>

                <Details
                    dictionary={dictionary}
                    analytics={analytics}/>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    dictionary: getDictionary(state),
    analytics: getSiteAnalytics(state),
    url: getUrl(state),
    keyWords: getKeyWords(state)
});

export default connect(mapStateToProps)(SiteAnalyticsResults);