import keyMirror from 'key-mirror';

export const analysisTypes = keyMirror({
    SITE: null,
    QUERY: null
});

export const aggregateFunctions = keyMirror({
    MAX: null
});

export const routes = {
    INDEX: '/',
    SITE: '/analyze-site',
    SEARCH_QUERY: '/analyze-search-query',
    SITE_RESULTS: '/analyze-site-results',
    QUERY_RESULTS: '/analyze-query-results'
};

export const paths = {
    ANALYZE_SITE: '/analysis/site',
    ANALYZE_QUERY: '/analysis/query'
};

export const server = {
    protocol: 'http',
    host: 'localhost',
    port: '8070'
};

export const errors ={
    REQUIRED: 'required',
    INVALID_URL: 'invalidUrl',
    ANALYSIS_ERROR: 'analysisError'
};