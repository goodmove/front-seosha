import React from 'react';
import ReactDOM from 'react-dom';

import { reducers } from 'src/store/Reducers';
import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { routes } from "src/model/Model";
import { Route, Switch } from "react-router";
import SiteAnalyticsScreen from "src/container/site/SiteAnalyticsScreen";
import QueryAnalyticsScreen from "src/container/query/QueryAnalyticsScreen";
import Toolbar from "src/container/common/Toolbar";
import MainPage from "src/container/common/MainPage";
import SiteAnalyticsResultsScreen from "src/container/site/SiteAnalyticsResultsScreen";
import QueryAnalyticsResultsScreen from "src/container/query/QueryAnalyticsResultsScreen";

const store = createStore(combineReducers(reducers), applyMiddleware(thunk));

const App = () => (
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                {
                    [routes.SITE, routes.SEARCH_QUERY, routes.SITE_RESULTS, routes.QUERY_RESULTS]
                        .map(route =>
                            <Route key={route} path={route} component={Toolbar}/>
                        )
                }
            </Switch>
            <Switch>
                <Route exact path={routes.INDEX} component={MainPage}/>
                <Route exact path={routes.SITE} component={SiteAnalyticsScreen}/>
                <Route exact path={routes.SEARCH_QUERY} component={QueryAnalyticsScreen}/>
                <Route exact path={routes.SITE_RESULTS} component={SiteAnalyticsResultsScreen}/>
                <Route exact path={routes.QUERY_RESULTS} component={QueryAnalyticsResultsScreen}/>
            </Switch>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(<App/>, document.getElementById('app'));

module.hot.accept();