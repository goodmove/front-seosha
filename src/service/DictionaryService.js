class DictionaryService {
    static getDictionary() {
        return {
            logo: 'Сеоша',
            siteAnalysis: 'Анализ сайта',
            queryAnalysis: 'Анализ результатов поиска',
            analyzedSites: 'Исследованные сайты',
            nauseaAnalysis: 'Анализ тошнотности текста',
            siteUrl: 'URL сайта',
            insertSiteUrl: 'Введите URL сайта для анализа',
            keywords: 'Ключевые слова',
            otherWords: 'Другие слова',
            search: 'Найти',
            searchQuery: 'Поисковый запрос',
            weAnalyzeYandexTopSearchResults: 'Мы анализируем топ поисковой выдачи Яндекса',
            learnByWhichWordsTheyFindSite: 'Анализ текста на сайте',
            whatToAnalyze: 'Что исследуем?',
            concreteSite: 'Использовать конкретный сайт',
            topSearchResults: 'Использовать результаты поисковика',
            youWillLearn: 'Что анализирует сеоша',
            notSpecified: '-',
            numberOfOccurrences: 'Количество вхождений (словоформы / точное совпадение)',
            sameRoots: 'Словоформы',
            word: 'Слово',
            showDetails: 'Показать детали',
            results: 'Результаты',
            invalidUrl: 'Неверный URL',
            required: 'Обязательно к заполнению',
            analysisError: 'Ошибка в ходе анализа сайта',
            summary: 'Общий анализ (максимум среди сайтов)',
            detailsFromEachSite: 'Детали с каждого сайта'
            unexpectedError: 'Неожиданная ошибка',
            siteAnalysisDescription: 'Детальный анализ заданного сайта. Построим полный словарь словоформ, ' +
                'используемых на странице, с возможностью фильтрации по степени важности.',
            queryAnalysisDescription: 'Найдем сходства и различия на страницах лидеров выборки поисковой системы ' +
                'по интересующему вас запросу.',
            nauseaAnalysisDescription: 'Поможем привести текст к удобной для восприятия форме за счет замены “тошнотных” ' +
                'слов синонимами без вреда для индексации страницы.',
            research: 'Исследовать'
        }
    }
}


export default DictionaryService;