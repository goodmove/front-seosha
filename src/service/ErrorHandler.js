import DictionaryService from "src/service/DictionaryService";

const dictionary = DictionaryService.getDictionary();

class ErrorHandler {
    static handleError(error) {
        if (error.code !== undefined) {
            alert(dictionary[error.code])
        } else {
            alert(dictionary.unexpectedError)
        }
    }
}

export default ErrorHandler;