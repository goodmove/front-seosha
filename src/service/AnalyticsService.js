import axios from 'axios';
import { types as siteTypes } from "src/store/site/Types";
import { types as queryTypes } from "src/store/query/Types";
import { aggregateFunctions, paths, server } from "src/model/Model";
import ErrorHandler from "src/service/ErrorHandler";

const forDevOnly = `${server.protocol}://${server.host}:${server.port}`;

const axiosInstance = axios.create({
    baseURL: `/api`,
    headers: { 'Content-Type': 'application/json' }
});

class AnalyticsService {
    static analyzeSite(dispatch, url, keys) {
        return axiosInstance.post(paths.ANALYZE_SITE, { url, keys })
            .then(response => dispatch({ type: siteTypes.SITE_ANALYTICS_FETCHED, analytics: response.data }))
            .catch(ErrorHandler.handleError);
    }

    static analyzeQuery(dispatch, searchQuery, aggregateFunction = aggregateFunctions.MAX) {
        return axiosInstance.post(paths.ANALYZE_QUERY, { searchQuery, aggregateFunction })
            .then(response => {
                dispatch({ type: queryTypes.QUERY_ANALYTICS_FETCHED, analytics: response.data })
            })
            .catch(ErrorHandler.handleError);
    }
}

export default AnalyticsService;