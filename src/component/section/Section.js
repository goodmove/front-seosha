import React from "react";
import styles from "./Section.scss";

export const Section = ({ children }) => <div className={styles.section}>{children}</div>;