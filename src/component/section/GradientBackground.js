import React from "react";

import styles from "./GradientBackground.scss";

export const GradientBackground = ({ children }) => (
    <div className={styles.gradientBackground}>{children}</div>
);