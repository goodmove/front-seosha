import React from "react";

import styles from "./Card.scss";

export const Card = ({children}) => (
    <div className={styles.card}>{children}</div>
);