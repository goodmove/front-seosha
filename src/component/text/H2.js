import React from "react";

import styles from "./H2.scss";

export const H2 = ({ children }) => (
    <h2 className={styles.h2}>{children}</h2>
);