import React from "react";

import styles from "./H1.scss";

export const H1 = ({ children }) => (
    <h1 className={styles.h1}>{children}</h1>
);