import React from "react";

export const HeaderRow = (props) => (
    <tr>{props.children}</tr>
);

export const HeaderCell = (props) => (
    <th colSpan={props.colSpan}>{props.children}</th>
);

export const Row = (props) => (
    <tr>{props.children}</tr>
);

export const Cell = (props) => (
    <td>{props.children}</td>
);

export const Table = (props) => (
    <table>
        <tbody>{props.children}</tbody>
    </table>
);
