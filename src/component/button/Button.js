import * as React from "react";
import styles from "./Button.scss";

export const types = {
    BUTTON: 'button',
    SUBMIT: 'submit',
    RESET: 'reset'
};

const noOperation = () => {
};


const Button = ({ text = '', onClick = noOperation, type = types.BUTTON, disabled = false }) => (
    <button disabled={disabled} className={styles.button} type={type} onClick={onClick}>{text}</button>
);

export default Button;