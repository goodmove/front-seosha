import * as React from "react";
import styles from "./Fields.scss";

export const TextField = (
    {
        input,
        label,
        dictionary,
        meta: { touched, error },
        placeholder
    }) => {
    return (
        <div>
            {
                label && <label>{label}</label>
            }
            <div>
                <div>
                    <input {...input} placeholder={placeholder}/>
                </div>
                <div>
                    {
                        touched && error && <span className={styles.error}>{dictionary[error]}</span>
                    }
                </div>
            </div>
        </div>
    );
};